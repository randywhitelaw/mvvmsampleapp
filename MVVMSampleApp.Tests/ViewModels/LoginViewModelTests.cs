﻿using MVVMSampleApp.Models;
using MVVMSampleApp.Tests.Mocks;
using MVVMSampleApp.ViewModels;
using MVVMSampleApp.Views;
using NUnit.Framework;
using Slyno.MVVMToolkit;

namespace MVVMSampleApp.Tests.ViewModels
{
    [TestFixture]
    public class LoginViewModelTests
    {
        [Test]
        public async void LoginWithInvalidCredentials_ShouldSetSessionStateCurrentUserToNull()
        {
            var vm = new LoginViewModel
            {
                Username = "?",
                Password = "?",
            };

            AttachMockServices(vm);

            //set the session state to something
            vm.SessionStateService.CurrentUser = new User();

            await ((AsyncRelayCommand)vm.LoginCommand).ExecuteAsync(null);

            Assert.IsNull(vm.SessionStateService.CurrentUser);
        }

        [Test]
        public async void LoginWithValidCredentials_ShouldSetSessionStateCurrentUser()
        {
            var vm = new LoginViewModel
            {
                Username = "admin",
                Password = "admin",
            };

            AttachMockServices(vm);

            await ((AsyncRelayCommand)vm.LoginCommand).ExecuteAsync(null);

            Assert.IsNotNull(vm.SessionStateService.CurrentUser);
        }

        [Test]
        public async void LoginWithValidCredentials_ShouldNavigateToUsersView()
        {
            var vm = new LoginViewModel
            {
                Username = "admin",
                Password = "admin",
            };

            AttachMockServices(vm);

            await ((AsyncRelayCommand)vm.LoginCommand).ExecuteAsync(null);

            var currentViewType = vm.NavigationService.GetCurrentViewType();
            Assert.AreEqual(typeof(UsersView), currentViewType);
        }


        private static void AttachMockServices(ViewModel vewModel)
        {
            var navigationService = new MockNavigationService();
            var mockSessionStateService = new MockSessionStateService();
            var persistanceService = new MockPersistenceService();
            var exceptionService = new MockExceptionService();
            var dialogService = new MockDialogService();


            vewModel.SessionStateService = mockSessionStateService;
            vewModel.PersistenceService = persistanceService;
            vewModel.DialogService = dialogService;
            vewModel.NavigationService = navigationService;
            vewModel.ExceptionService = exceptionService;
        }

    }
}
