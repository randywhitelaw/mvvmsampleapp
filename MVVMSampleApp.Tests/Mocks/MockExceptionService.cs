﻿using System.Diagnostics;
using MVVMSampleApp.Interfaces;

namespace MVVMSampleApp.Tests.Mocks
{
    sealed class MockExceptionService : IExceptionService
    {
        public void LogException(System.Exception exception)
        {
            Debug.WriteLine("Exception Logged: {0}", exception);
        }
    }
}
