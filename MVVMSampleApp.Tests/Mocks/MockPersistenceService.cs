﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MVVMSampleApp.Interfaces;
using MVVMSampleApp.Models;

namespace MVVMSampleApp.Tests.Mocks
{
    sealed class MockPersistenceService : IPersistenceService
    {
        public async Task<IEnumerable<User>> LoadUsersAsync()
        {
            await Task.Run(() => Debug.WriteLine("LoadUsersAsync called."));

            return new List<User>
            {
                new User {Age = 50, FirstName = "Test1", LastName = "User"},
                new User {Age = 25, FirstName = "Test2", LastName = "User"},
                new User {Age = 16, FirstName = "Test3", LastName = "User"},
            };
        }

        public async Task<IEnumerable<User>> SearchUsersAsync(string query)
        {
            Debug.WriteLine("SearchUsersAsync called.");

            var users = await this.LoadUsersAsync();

            return users.Where(x => x.FirstName.Contains(query) || x.LastName.Contains(query) || x.Age.ToString() == query);
        }

        public async Task AddUserAsync(User user)
        {
            await Task.Run(() => Debug.WriteLine("AddUserAsync called."));
        }

        public async Task DeleteUserAsync(User user)
        {
            await Task.Run(() => Debug.WriteLine("AddUserAsync called."));
        }

        public Task<User> LoginAsync(string username, string password)
        {
            Debug.WriteLine("AddUserAsync called.");

            if (username == "admin" && password == "admin")
            {
                return Task.FromResult(new User { FirstName = "Admin", LastName = "User", Age = 50 });
            }

            return null;
        }

        public async Task LogoutAsync()
        {
            await Task.Run(() => Debug.WriteLine("AddUserAsync called."));
        }
    }
}
