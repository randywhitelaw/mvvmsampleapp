﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MVVMSampleApp.Interfaces;

namespace MVVMSampleApp.Tests.Mocks
{
    sealed class MockNavigationService : INavigationService
    {
        private Type _currentViewType;
        private int _currentViewIndex;
        private readonly List<Type> _backStack = new List<Type>();

        public Type GetCurrentViewType()
        {
            return _currentViewType;
        }

        public void Navigate(Type view)
        {
            this.Navigate(view, null);
        }

        public void Navigate(Type view, object viewData)
        {
            _currentViewType = view;
            _backStack.Add(view);
            _currentViewIndex = _backStack.Count - 1;

            var output = new StringBuilder();
            output.AppendFormat("Navigated to {0}", view.Name);
            if (viewData != null)
            {
                output.AppendFormat(" with data: {0}", viewData);
            }
            output.Append(".");

            Debug.WriteLine(output);
        }

        public void GoBack()
        {
            _currentViewIndex--;
            if (_currentViewIndex < 0)
            {
                Debug.WriteLine("Can't navigate back anymore.");
                return;
            }

            var name = _backStack[_currentViewIndex].Name;

            Debug.WriteLine(string.Format("Navigated back to {0}.", name));
        }
    }
}
