﻿using System.Diagnostics;
using MVVMSampleApp.Interfaces;
using MVVMSampleApp.Models;

namespace MVVMSampleApp.Tests.Mocks
{
    sealed class MockSessionStateService : ISessionStateService
    {
        private User _currentUser;

        public User CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                Debug.WriteLine("CurrentUser set: {0}", value);
            }
        }
    }
}
