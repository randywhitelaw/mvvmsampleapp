﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.UI.Popups;
using MVVMSampleApp.Interfaces;

namespace MVVMSampleApp.Tests.Mocks
{
    sealed class MockDialogService : IDialogService
    {
        public async Task ShowAsync(string message)
        {
            await Task.Run(() => Debug.WriteLine("Dialog: {0}", message));
        }

        public async Task ShowAsync(string message, IEnumerable<UICommand> uiCommands)
        {
            await Task.Run(() =>
            {
                Debug.WriteLine("Dialog: {0}", message);
                foreach (var uiCommand in uiCommands)
                {
                    Debug.WriteLine("Dialog Command Option: {0}", uiCommand.Label);
                }
            });
        }
    }
}
