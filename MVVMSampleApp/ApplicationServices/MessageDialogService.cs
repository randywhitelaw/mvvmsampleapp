﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI.Popups;
using MVVMSampleApp.Interfaces;

namespace MVVMSampleApp.ApplicationServices
{
    sealed class MessageDialogService : IDialogService
    {
        public async Task ShowAsync(string message)
        {
            var msg = new MessageDialog(message);
            await msg.ShowAsync();
        }

        public async Task ShowAsync(string message, IEnumerable<UICommand> uiCommands)
        {
            var msg = new MessageDialog(message);

            foreach (var uiCommand in uiCommands)
            {
                msg.Commands.Add(uiCommand);
            }

            await msg.ShowAsync();
        }
    }
}
