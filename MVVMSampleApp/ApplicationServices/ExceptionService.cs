﻿using System;
using MVVMSampleApp.Interfaces;

namespace MVVMSampleApp.ApplicationServices
{
    sealed class ExceptionService : IExceptionService
    {
        public void LogException(Exception exception)
        {
            //todo: log this to a text file or email, etc...
        }
    }
}
