﻿using MVVMSampleApp.Interfaces;
using MVVMSampleApp.Models;

namespace MVVMSampleApp.ApplicationServices
{
    sealed class SessionStateService : ISessionStateService
    {
        public User CurrentUser { get; set; }
    }
}
