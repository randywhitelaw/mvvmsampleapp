﻿using System;
using Windows.UI.Xaml.Controls;
using MVVMSampleApp.Interfaces;

namespace MVVMSampleApp.ApplicationServices
{
    sealed class FrameNavigationService : INavigationService
    {
        private readonly Frame _frame;

        public FrameNavigationService(Frame frame)
        {
            _frame = frame;
        }

        public Type GetCurrentViewType()
        {
            return _frame.CurrentSourcePageType;
        }

        public void Navigate(Type view)
        {
            _frame.Navigate(view);
        }

        public void Navigate(Type view, object viewData)
        {
            _frame.Navigate(view, viewData);
        }

        public void GoBack()
        {
            _frame.GoBack();
        }
    }
}
