﻿using System;
using Newtonsoft.Json;
using Slyno.MVVMToolkit;

namespace MVVMSampleApp.Models
{
    internal sealed class User : NotifyObject
    {
        public string FirstName
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public string LastName
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public int? Age
        {
            get { return GetPropertyValue<int?>(); }
            set
            {
                if (value.HasValue && (value < 13 || value > 99))
                {
                    throw new Exception("Age must be between 13 and 99.");
                }

                SetPropertyValue(value);
            }
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
