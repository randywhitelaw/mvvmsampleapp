﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MVVMSampleApp.ViewModels;

namespace MVVMSampleApp.Views
{
    public sealed partial class LoginView : Page
    {
        private readonly LoginViewModel _viewModel;

        public LoginView()
        {
            this.InitializeComponent();

            _viewModel = (LoginViewModel)this.DataContext;
        }

        private void Login_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.Password = PasswordBox.Password;
            _viewModel.LoginCommand.Execute(null);
        }
    }
}
