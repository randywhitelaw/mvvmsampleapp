﻿using System;
using System.Windows.Input;
using MVVMSampleApp.Interfaces;
using MVVMSampleApp.Views;
using Slyno.MVVMToolkit;

namespace MVVMSampleApp.ViewModels
{
    abstract class ViewModel : NotifyObject
    {
        private readonly ICommand _logoutCommand;

        protected ViewModel()
        {
            this.DialogService = App.DialogService;
            this.ExceptionService = App.ExceptionService;
            this.NavigationService = App.NavigationService;
            this.PersistenceService = App.PersistenceService;
            this.SessionStateService = App.SessionStateService;

            _logoutCommand = new AsyncRelayCommand(async obj =>
            {
                this.IsLoading = true;
                try
                {
                    await this.PersistenceService.LogoutAsync();
                }
                catch (Exception ex)
                {
                    this.ExceptionService.LogException(ex);
                }
                finally
                {
                    this.IsLoading = false;
                    this.NavigationService.Navigate(typeof(LoginView));
                }
            });

            this.GoBackCommand = new RelayCommand(obj => this.NavigationService.GoBack());
        }

        public bool IsLoading
        {
            get { return GetPropertyValue<bool>(); }
            set { SetPropertyValue(value); }
        }

        public IDialogService DialogService { get; set; }

        public IExceptionService ExceptionService { get; set; }

        public INavigationService NavigationService { get; set; }

        public IPersistenceService PersistenceService { get; set; }

        public ISessionStateService SessionStateService { get; set; }

        public ICommand GoBackCommand { get; set; }

        public ICommand LogoutCommand
        {
            get { return _logoutCommand; }
        }
    }
}
