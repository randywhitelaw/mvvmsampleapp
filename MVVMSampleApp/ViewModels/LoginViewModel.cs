﻿using System;
using System.Windows.Input;
using MVVMSampleApp.Views;
using Slyno.MVVMToolkit;

namespace MVVMSampleApp.ViewModels
{
    sealed class LoginViewModel : ViewModel
    {
        private readonly ICommand _loginCommand;

        public LoginViewModel()
        {
            _loginCommand = new AsyncRelayCommand(async obj =>
            {
                this.IsLoading = true;

                this.SessionStateService.CurrentUser = null;

                try
                {
                    this.SessionStateService.CurrentUser = await this.PersistenceService.LoginAsync(this.Username, this.Password);
                }
                catch (Exception ex)
                {
                    this.ExceptionService.LogException(ex);
                }
                finally
                {
                    this.IsLoading = false;
                }

                if (this.SessionStateService.CurrentUser == null)
                {
                    await this.DialogService.ShowAsync("Invalid login information.");
                }
                else
                {
                    this.NavigationService.Navigate(typeof(UsersView));
                }
            });
        }

        public string Username
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public string Password
        {
            get { return GetPropertyValue<string>(); }
            set { SetPropertyValue(value); }
        }

        public ICommand LoginCommand
        {
            get { return _loginCommand; }
        }
    }
}
