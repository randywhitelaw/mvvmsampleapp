﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MVVMSampleApp.Models;
using MVVMSampleApp.Views;
using Slyno.MVVMToolkit;

namespace MVVMSampleApp.ViewModels
{
    sealed class UsersViewModel : ViewModel
    {
        private readonly ICommand _loadUsersCommand;
        private readonly ICommand _addUserCommand;
        private readonly ICommand _viewUserCommand;
        private readonly ObservableCollection<User> _users = new ObservableCollection<User>();

        public UsersViewModel()
        {
            _loadUsersCommand = new AsyncRelayCommand(async obj =>
            {
                this.IsLoading = true;

                _users.Clear();

                try
                {
                    var items = await this.PersistenceService.LoadUsersAsync();
                    foreach (var item in items)
                    {
                        _users.Add(item);
                    }
                }
                catch (Exception ex)
                {
                    this.ExceptionService.LogException(ex);
                }
                finally
                {
                    this.IsLoading = false;
                }

                if (_users.Count == 0)
                {
                    await this.DialogService.ShowAsync("No users available.");
                }
            });

            _addUserCommand = new RelayCommand(obj => this.NavigationService.Navigate(typeof(AddUserView)));

            _viewUserCommand = new RelayCommand(obj => this.NavigationService.Navigate(typeof(UserView), this.SelectedUser));
        }

        public User SelectedUser
        {
            get { return GetPropertyValue<User>(); }
            set { SetPropertyValue(value); }
        }

        public ObservableCollection<User> Users
        {
            get { return _users; }
        }

        public ICommand LoadUsersCommand
        {
            get { return _loadUsersCommand; }
        }

        public ICommand AddUserCommand
        {
            get { return _addUserCommand; }
        }

        public ICommand ViewUserCommand
        {
            get { return _viewUserCommand; }
        }
    }
}
