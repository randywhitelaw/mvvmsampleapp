﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MVVMSampleApp.Models;

namespace MVVMSampleApp.Interfaces
{
    interface IPersistenceService
    {
        Task<IEnumerable<User>> LoadUsersAsync();

        Task<IEnumerable<User>> SearchUsersAsync(string query);

        Task AddUserAsync(User user);

        Task DeleteUserAsync(User user);

        Task<User> LoginAsync(string username, string password);

        Task LogoutAsync();
    }
}
