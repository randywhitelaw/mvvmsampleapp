﻿using System;

namespace MVVMSampleApp.Interfaces
{
    interface INavigationService
    {
        Type GetCurrentViewType();

        void Navigate(Type view);

        void Navigate(Type view, object viewData);

        void GoBack();
    }
}
