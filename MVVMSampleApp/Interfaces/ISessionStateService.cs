﻿using MVVMSampleApp.Models;

namespace MVVMSampleApp.Interfaces
{
    interface ISessionStateService
    {
        User CurrentUser { get; set; }
    }
}
