﻿using System;

namespace MVVMSampleApp.Interfaces
{
    interface IExceptionService
    {
        void LogException(Exception exception);
    }
}
