﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace MVVMSampleApp.Interfaces
{
    interface IDialogService
    {
        Task ShowAsync(string message);

        Task ShowAsync(string message, IEnumerable<UICommand> uiCommands);
    }
}
